#!/bin/bash                                                                                                                                                       
                                                                                                                                                                  
if [ "$1" == "" ] || [ $# -gt 1 ]; then                                                                                                                           
        echo "Input token is empty. Please provide logz.io token"                                                                                                 
        exit 0                                                                                                                                                    
fi                                                                                                                                                                
                                                                                                                                                                  
wget https://raw.githubusercontent.com/logzio/public-certificates/master/COMODORSADomainValidationSecureServerCA.crt                                              
sudo mkdir -p /etc/pki/tls/certs                                                                                                                                  
sudo cp COMODORSADomainValidationSecureServerCA.crt /etc/pki/tls/certs/                                                                                           
                                                                                                                                                                  
wget https://gitlab.com/npaskov/logzio/raw/master/filebeat-logz                                                                                                   
wget https://gitlab.com/npaskov/logzio/raw/master/filebeat-logz.conf                                                                                              
                                                                                                                                                                  
cat filebeat-logz.conf | sed s/"token: "/"token: $1"/g > filebeat-logz.yml                                                                                        
                                                                                                                                                                  
sudo cp filebeat-logz /etc/init.d/                                                                                                                                
chmod +x /etc/init.d/filebeat-logz                                                                                                                                
sudo cp filebeat-logz.yml /etc/filebeat/                                                                                                                          
                                                                                                                                                                  
sudo /etc/init.d/filebeat-logz start 
sudo update-rc.d filebeat-logz defaults